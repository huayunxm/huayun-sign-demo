const CryptoJS = require('crypto-js')

//需要填写部分 begin
const action  = ""  
const accessKeyId = ""
const accessKeySecret = ""
//需要填写部分 end

const version = "1.0"
const contentType = "application/json;charset=UTF-8"
const time = new Date()
var year = time.getFullYear()
var month = time.getMonth()
var date = time.getDate()
var hour = time.getHours()
var min = time.getMinutes()
var sec = time.getSeconds()
const headerTime = new Date().toLocaleDateString().replaceAll('/', '-') + 'T' + new Date().toTimeString().replace(/[^\d:\s+]/g, '').trim()
const queryTime = encodeURIComponent(headerTime)

function getSignature() {
    const enStr = `Action=${action}&Version=${version}&AccessKeyId=${accessKeyId}&Date=${queryTime}`
    const sbParams = CryptoJS.MD5(enStr).toString()
    const data = `POST\n${sbParams}\n${contentType}\n${queryTime}\n`
    const encode = CryptoJS.HmacSHA256(CryptoJS.enc.Utf8.parse(data), CryptoJS.enc.Utf8.parse(`${accessKeySecret}`), true)
    const base64 = encode.toString(CryptoJS.enc.Base64) 
    let res = encodeURIComponent(base64)
    res = res.replace(/\+/g, '%20')
    res = res.replace(/\*/g, '%2A')
    res = res.replace(/%7E/g, '~')
    return res
}

